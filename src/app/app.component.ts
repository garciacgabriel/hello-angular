import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  
  message = 'Hello, Angular. Ticaricatica'
  changed = false

  changeMessage(){
    this.message = this.changed ? 'Hello, Angular. Ticaricatica' : 'FALA DELES HELLO ANGULAR'
    this.changed = !this.changed
  }
}